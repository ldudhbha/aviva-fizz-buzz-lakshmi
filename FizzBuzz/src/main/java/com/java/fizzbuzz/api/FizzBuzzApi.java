package com.java.fizzbuzz.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.java.fizzbuzz.model.FizzBuzz;
import com.java.fizzbuzz.service.FizzBuzzServiceImpl;

@Path("/")
public class FizzBuzzApi {
	FizzBuzzServiceImpl buzzServiceImpl = new FizzBuzzServiceImpl();
	@GET
	@Produces(MediaType.TEXT_XML)
	@Path("{number}")
	public FizzBuzz getNumbers(@PathParam("number") Integer number) {
		try {
	        FizzBuzz fizzBuzz = new FizzBuzz();
	        fizzBuzz.getFizzBuzz().addAll(buzzServiceImpl.getFizzBuzz(number));
	        return fizzBuzz;
	    } catch (Exception e) {
	   
	        throw new RuntimeException(e.getMessage());
	    }


	}
}
