package com.java.fizzbuzz.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FizzBuzzServiceImpl{

	public List<String> getFizzBuzz(Integer number) {
		List<String> fizzBuzzList = null;
		Date now = new Date();
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
		String day = simpleDateformat.format(now);
		if (number > 0 && number <= 1000) {
			fizzBuzzList = new ArrayList<String>();
			for (int i = 1; i <= number; i++) {
				// number divisible by 3, print 'Fizz'
				if (i % 3 == 0) {
					if (day == "Wednesday") {
						fizzBuzzList.add("wizz");
					} else {
						fizzBuzzList.add("fizz");
					}
				}
				// number divisible by 5, print 'Buzz'
				else if (i % 5 == 0) {
					if (day == "Wednesday") {
						fizzBuzzList.add("wuzz");
					} else {
						fizzBuzzList.add("buzz");
					}
				}
				// divisible by both 15, print 'FizzBuzz'
				else if (i % 15 == 0) {
					fizzBuzzList.add("fizz buzz");
				}
				// print the numbers
				else {
					fizzBuzzList.add(String.valueOf(i));
				}
			}

		}
		return fizzBuzzList;

	}

}
